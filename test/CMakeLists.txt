cmake_minimum_required(VERSION 3.24)
project(pdre_test C)

set(CMAKE_C_STANDARD 23)
set(PDRE_TEST_SRC_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src)

include_directories(${PDRE_INC_DIR})
aux_source_directory(${PDRE_TEST_SRC_DIR} PDRE_SRC)

#add_compile_definitions("XCHAR_USING_CHAR")
#add_compile_definitions("XCHAR_USING_WCHAR")

add_executable(function-test
        test-suites.h test-suites.c
        test.c)

target_link_libraries(function-test check)