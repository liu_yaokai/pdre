/*
MIT License

Copyright (c) 2022 Yaokai Liu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
//
// Created by Yaokai Liu on 22-10-15.
//
#include "parse.h"
#include "alloc.h"
#include "xstring.h"
#include "test-suites.h"

#define len(a)      (sizeof(a) / sizeof(xChar) - 1)

START_TEST(test_escape_process) {
    xSize size1 = 0;
    token ** token1 = NULL;
    token1 = tokenize(char_s "\\{", &size1);
    fail_unless(token1, "No token list returned!");
    fail_unless(size1 == 1, "Length of token list is not correct!");
    fail_unless(token1[0]->catcode == plain, "Error occurs in function `escape_process'!");
}
END_TEST

START_TEST(test_alias_process) {
    xSize size1 = 0;
    token ** token1 = NULL;
    xSize p = 0;

    token1 = tokenize(char_s "*", &size1);
    fail_unless(token1, "No token list returned!");
    fail_unless(size1 == len(char_s "{,}"), "Length of token list is not correct!");
    p = strcmp(show_processed_rexp(token1, size1), "{,}");
    fail_unless(p == 0, "Error occurs when call `alias_process'!");
}
END_TEST


START_TEST(test_default_process) {
    xSize size1 = 0;
    token ** token1 = NULL;
    xSize p = 0;

    token1 = tokenize(LOWER_LETTERS, &size1);
    fail_unless(token1, "No token list returned!");
    fail_unless(size1 == len(LOWER_LETTERS), "Length of token list is not correct!");
    p = strcmp(show_processed_rexp(token1, size1), LOWER_LETTERS);
    fail_unless(p == 0, "Error occurs when call `alias_process'!");
}
END_TEST


START_TEST(test_tokenize) {
    xSize size1 = 0;
    token ** token1 = NULL;
    token1 = tokenize(xChar(""), &size1);
    fail_unless(token1, "No token list returned!");
    fail_unless(size1 == 0, "Length of token list is not correct!");
    fail_unless(token1[0] == NULL, "Tokenize null string but doesn't return null list!");
    pdre_free(token1)
}
END_TEST




Suite * check_tokenize_suite() {
    Suite * s = suite_create("tokenize");
    TCase * tCase = tcase_create("tokenize");
    suite_add_tcase(s, tCase);

    tcase_add_test(tCase, test_escape_process);
    tcase_add_test(tCase, test_alias_process);
    tcase_add_test(tCase, test_default_process);
    tcase_add_test(tCase, test_tokenize);

    return s;
}