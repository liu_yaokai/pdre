# PDRE

(This project has been moved to '[X-RE](https://gitlab.com/liu_yaokai/xre)', and will no langer update.)

This project is aimed to provide a **preciously described regular expression**, which mainly used in 
compiler's lex/parse stages. With a preciously express, the compiler could easily control the lex/parse behaviors,
and could easily fix its rules.

## Scope
This project will specify:

1. The grammar of parse a pd-regular expression;
2. The match, replace behavior when apply a pd-regular expression.

This project wil not specify:

1. The way to apply a pd-regular expression, means no matter if using compiled program or interrupter.

## License & Copyright

This project is using MIT license, with Yaokai Liu's copyright from 2022.