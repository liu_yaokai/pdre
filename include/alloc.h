/*
MIT License

Copyright (c) 2022 Yaokai Liu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
//
// Created by Yaokai Liu on 22-10-10.
//

#ifndef PDpdre_realloc_H
#define PDpdre_realloc_H

#ifdef DEBUG
#include "xtypes.h"
#include "xdef.h"
#include <malloc.h>

typedef struct _MemEntity MemEntity;

typedef struct _MemEntity {
    char *      file;
    xSize      line;
    char *      name;
    void *      addr;
    xSize      size;
    int         is_free;
    MemEntity * next;
} MemEntity;

extern const MemEntity * const MemEntityHeader;
extern MemEntity * CurMemEntity;
extern xSize total_mem_entity;

void * __pdre_malloc(char * _var, xSize _size, char * file, xSize line);
void * __pdre_calloc(char * _var, xSize _nmemb,xSize _size, char * file, xSize line);
void *__pdre_realloc(char * _var1, void * src, xSize _size, char * file, size_t line);
void __pdre_free(void * _var);

#define pdre_malloc(_var, _size)            _var = __pdre_malloc(#_var, _size, __FILE__, __LINE__);
#define pdre_calloc(_var, _nmemb, _size)    _var = __pdre_calloc(#_var, _nmemb, _size, __FILE__, __LINE__);
#define pdre_realloc(_var1, _var2, _size)   _var1 = __pdre_realloc(#_var1, _var2, _size, __FILE__, __LINE__);
#define pdre_free(_var)                     __pdre_free(_var);

void print_all_entity();
void print_unfree_entity();
#else
#include <malloc.h>
#define pdre_malloc(_var, _size)            _var = malloc(_size);
#define pdre_calloc(_var, _nmemb, _size)    _var = calloc(_nmemb, _size);
#define pdre_realloc(_var1, _var2, _size)   _var1 = realloc(_var2, _size);
#define pdre_free(_var)                     free(_var);
#endif

#endif //PDpdre_realloc_H
