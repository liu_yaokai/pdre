/*
MIT License

Copyright (c) 2022 Yaokai Liu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Yaokai Liu on 22-9-29.
//
/*
 * This file specifies objects of PDRE scope.
 */

#ifndef PDRE2C_OBJECTS_H
#define PDRE2C_OBJECTS_H

#include "catcode.h"
#include "xchar.h"

#define ARRAY_ALLOC_SIZE    64

typedef struct {
    catcode     catcode;
    xChar      value[1];
} token;

typedef enum {
    SEQ = 1,
    RNG,
    SET,
    GRP,
    UNI,
    TOP,

    CNT,
    VAR
} obj_type;

typedef struct {
    obj_type    id;
    xBool       only_parse;
    xBool       is_inverse;
} ReObj;

typedef struct {
    obj_type    id;
    xBool       only_parse;
    xBool       is_inverse;
    xSize      len;
    xChar *    value;
} Seq;

typedef struct {
    obj_type    id;
    xChar      start;
    xChar      end;
} Range;

typedef struct {
    obj_type    id;
    xBool       only_parse;
    xBool       is_inverse;
    xChar *    rexp;
    xSize      n_plains;
    xChar *    plain;
    xSize      n_ranges;
    Range **    ranges;
} Set;

typedef struct {
    obj_type    id;
    xBool       only_parse;
    xBool       is_inverse;
    xChar *    rexp;
    xSize      n_sub;
    ReObj **    subObjs;
} Group;

typedef struct {
    obj_type    id;
    xBool       only_parse;
    xBool       is_inverse;
    ReObj *     LHS;
    Group *     RHS;
} Union;

typedef struct {
    obj_type    id;
    xBool       only_parse;
    xBool       is_inverse;
    xSize      min;
    xSize      max;
    ReObj *     obj;
} Count;

typedef struct {
    obj_type    id;
    xBool         only_parse;
    xBool         is_inverse;
    xChar *    value;
    ReObj *     obj;
} Var;

typedef struct {
    obj_type    id;
    xChar *    rexp;
    xSize      n_sub;
    ReObj **    subObjs;
} Top;

xSize getVarCount();
xSize getGrpCount();
Var * getVar(xSize _index);
Group * getGrp(xSize _index);
xSize addVarToTable(Var * var);
xSize addGrpToTable(Group * grp);


void releasepTokenArray(token * tokens[]);
void releaseObj(void *);
void releaseVarTable();
void releaseGrpTable();

#endif //PDRE2C_OBJECTS_H
