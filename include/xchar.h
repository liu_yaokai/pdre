//
// Created by Yaokai Liu on 12/6/22.
//

#ifndef XC_CHAR_T_H
#define XC_CHAR_T_H

#include "xtypes.h"

#ifdef XCHAR_USING_CHAR
typedef char            xChar;
#elifdef XCHAR_USING_WCHAR
#ifdef __WCHAR_TYPE__
typedef __WCHAR_TYPE__  xChar;
#else
typedef int             xChar;
#endif
#else
#error "You should specify character type."
#endif

#ifdef XCHAR_USING_CHAR
#define xChar(x)    x
#define charS       "%s"

#elifdef XCHAR_USING_WCHAR
#define xChar(x)    L##x
#define charS       "%ls"

#else
#error "You should specify character type."
#endif

xuInt ord(xChar _the_char);
xChar chr(xuInt _the_order);

#endif //XC_CHAR_T_H
