//
// Created by Yaokai Liu on 11/29/22.
//

#ifndef X_XTYPES_H
#define X_XTYPES_H

#ifdef XTYPES_USING_STDC_TYPE
typedef void            xVoid;
typedef _Bool           xBool;

typedef signed char     xByte;
typedef signed short    xShort;
typedef signed int      xInt;
typedef signed long     xLong;

typedef unsigned char   xuByte;
typedef unsigned short  xuShort;
typedef unsigned int    xuInt;
typedef unsigned long   xuLong;


#ifdef __SIZE_TYPE__
typedef __SIZE_TYPE__   xSize;
#else
typedef unsigned long   xSize;
#endif
#endif

#endif //X_XTYPES_H
