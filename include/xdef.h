//
// Created by Yaokai Liu on 11/29/22.
//

#ifndef X_XDEF_H
#define X_XDEF_H

#include "xtypes.h"

#ifndef nullof
#define nullof(_TYPE)    ((_TYPE *) 0)
#endif

#ifdef XDEF_USING_C_STD_DEF
#ifndef NULL
#define NULL    nullof(xVoid)
#endif

#if !defined(true) || !defined(false)
#define true	((xBool)+1u)
#define false	((xBool)+0u)
#endif

#ifndef offsetof
#define offsetof(_TYPE, _MEMBER) ((xSize) &(nullof(_TYPE))->_MEMBER)
#endif
#endif

#endif //X_XDEF_H
