//
// Created by "Yaokai Liu" on 12/10/22.
//

#ifndef XC_XSTRING_H
#define XC_XSTRING_H

#include "xchar.h"

xSize strcpy(xChar * _dest, const xChar * _src, xChar _end_char);
xSize strlen(const xChar * _string, xChar _end_char);
xLong strcmp(const xChar * _str1, const xChar * _str2, xChar _end_char);
xSize str2ul(const xChar *_string, xuLong * result,
             xChar _end_char, xuLong _base);

#endif //XC_XSTRING_H
