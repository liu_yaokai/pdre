/*
MIT License

Copyright (c) 2022 Yaokai Liu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
//
// Created by Yaokai Liu on 22-10-12.
//

#ifndef PDRE_RUN_H
#define PDRE_RUN_H

#include "xchar.h"
#include "objects.h"

xBool  match_seq(const Seq * seq,     const xChar * src, xSize * __n_matched);
xBool  match_set(const Set * set,     const xChar * src, xSize * __n_matched);
xBool  match_grp(const Group * grp,   const xChar * src, xSize * __n_matched);
xBool  match_uni(const Union * uni,   const xChar * src, xSize * __n_matched);
xBool  match_cnt(const Count * cnt,   const xChar * src, xSize * __n_matched);
xBool  match_var(const Var * var,     const xChar * src, xSize * __n_matched);
xBool  match_obj(const void * obj,    const xChar * src, xSize * __n_matched);
xBool  match_top(const Top * top,     const xChar * src, xSize * __n_matched);

#endif //PDRE_RUN_H
