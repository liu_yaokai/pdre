/*
MIT License

Copyright (c) 2022 Yaokai Liu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Yaokai Liu on 22-9-29.
//
/*
 * This file specifies object categories of PDRE.
 */


#ifndef PDRE2C_CATCODE_H
#define PDRE2C_CATCODE_H

#include "xchar.h"

// category code of character
typedef enum {
    escape      = '\\',     // escape char: '\'

    beginG      = '(',      // begin of group : '('
    endG        = ')',      // end of group: ')'
    beginS      = '[',      // begin of set: '['
    endS        = ']',      // end of set: ']'
    beginC      = '{',      // begin of count: '{'
    endC        = '}',      // end of count: '}'
    beginV      = '<',      // begin of variable: '<'
    endV        = '>',      // end of variable: '>'

    rangeTO     = '-',      // range to: '-'

    comma       = ',',       // comma: ','
    unionOR     = '|',       // group union: '|'
    inverse     = '^',       // group inverse: '^'
    assign      = '=',       // group assign to variable: '='
    call        = '@',       // call variable: '@'
    numOf       = '#',       // value of variable: '#'
    condWith    = '~',       // match condition: '~'
    onlyParse   = '!',       // only parse but not execute: '!'

    alias       = 128,       // alias char like: '*', '+', '?', etc.

    plain       = 128 + 127,      // plain char
} catcode;

catcode get_catcode(xChar _dest);
xChar *get_alias(const xChar *_dest, xSize *offs);

#endif //PDRE2C_CATCODE_H
