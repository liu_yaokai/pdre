/*
MIT License

Copyright (c) 2022 Yaokai Liu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Yaokai Liu on 22-9-29.
//
/*
 * This file specifies parsing rules of PDRE string and their realizations.
 */

#ifndef PDRE2C_PARSE_H
#define PDRE2C_PARSE_H

#include "objects.h"
#include "xchar.h"
#include "catcode.h"

#define N_PREPROCESS 32
// #define N_RULES 128

// scope

token ** tokenize(const xChar *rexp, xSize * sizeof_result);
xChar * show_processed_rexp(token ** tokens, xSize size);
Top *    parse(token ** tokens);

Seq *parse_seq(token **, xSize *);
Range *parse_rng(token **, xSize *);
Set *parse_set(token **, xSize *);
Group *parse_grp(token **, xSize *);
Union *parse_uni_rhs(token **tokens, xSize *offs);
Count *parse_cnt(token **, xSize *);
Var *parse_var(token **, xSize *);


#endif //PDRE2C_PARSE_H
