/*
MIT License

Copyright (c) 2022 Yaokai Liu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Yaokai Liu on 22-9-29.
//

#include "parse.h"
#include "alloc.h"

typedef token **(*preprocess_func)(xChar *, xSize *, xSize *);

preprocess_func preprocess_funcs[N_PREPROCESS];

token **tokenize(const xChar *rexp, xSize *sizeof_result) {
    xChar *sp = (xChar *)rexp;

    // todo: tokens specify.
    xSize len = 0;
    while (sp[len]) len++;

    xSize n_rules = 0;
    while (preprocess_funcs[n_rules]) n_rules++;


    token **p = NULL;
    xSize n_tokens = 0;
    xSize n_new_tokens = 0;
    xSize token_buff_len = ARRAY_ALLOC_SIZE - 1;
    pdre_calloc(token **tokens, token_buff_len, sizeof(token *))
    xSize offset = 0;
    while (sp[0] != xChar('\0')) {
        for (xSize i = 0; i < n_rules; i++) {
            p = preprocess_funcs[i](sp, &offset, &n_new_tokens);
            if (n_new_tokens) {
                while (n_tokens + n_new_tokens >= token_buff_len) {
                    token_buff_len += ARRAY_ALLOC_SIZE;
                    pdre_realloc(tokens, tokens, (token_buff_len + 1) * sizeof(token *))
                }
                for (xSize j = 0; p[j]; j++) {
                    tokens[n_tokens + j] = p[j];
                }
                n_tokens += n_new_tokens;
                pdre_free(p)
                break;
            }
        }
        if (!n_new_tokens)
            goto __failed_tokenize;
        sp += offset;
    }
    *sizeof_result = n_tokens;
    tokens[*sizeof_result] = NULL;
    return tokens;

    __failed_tokenize:
    pdre_free(tokens)
    *sizeof_result = 0;
    return NULL;
}

token **escape_process(xChar *prexp, xSize *offs, xSize *n_tokens) {
    xChar *sp = prexp;
    if (get_catcode(sp[0]) != escape)
        goto __failed_tokening_escape;
    sp++;

    pdre_calloc(token *t, 1, sizeof(token))

    // TODO: Unicode escape support.
    // TODO: Special escape support.
    if (get_catcode(sp[0]) <= alias) {
        t->catcode = plain;
    } else {
        t->catcode = plain;
    }

    t->value[0] = sp[0];
    sp++;

    pdre_calloc(token **tokens, 2, sizeof(token *))
    tokens[0] = t;
    *offs = sp - prexp;
    *n_tokens = 1;
    return tokens;

    __failed_tokening_escape:
    *offs = 0;
    *n_tokens = 0;
    return NULL;
}

token **alias_process(xChar *prexp, xSize *offs, xSize *n_tokens) {
    xChar *sp = prexp;
    if (get_catcode(sp[0]) != alias)
        goto __failed_tokening_alias;
    xChar *asp = get_alias(sp, offs);
    if (!asp)
        goto __failed_tokening_alias;
    sp += *offs;

    token **tokens = tokenize(asp, n_tokens);
    if (!tokens)
        goto __failed_tokening_alias;
    *offs = sp - prexp;
    return tokens;

    __failed_tokening_alias:
    *offs = 0;
    *n_tokens = 0;
    return NULL;
}

token **default_process(xChar *prexp, xSize *offs, xSize *n_tokens) {
    xChar *sp = prexp;
    pdre_calloc(token *t, 1, sizeof(token))

    t->catcode = get_catcode(sp[0]);
    if (t->catcode == 0)
        goto __failed_process_default;
    t->value[0] = sp[0];
    sp++;

    pdre_calloc(token **tokens, 2, sizeof(token *))
    tokens[0] = t;
    *offs = sp - prexp;
    *n_tokens = 1;
    return tokens;

    __failed_process_default:
    *offs = 0;
    *n_tokens = 0;
    return NULL;
}

preprocess_func preprocess_funcs[N_PREPROCESS] = {
        escape_process,
        alias_process,
        default_process,
};

xChar *show_processed_rexp(token **tokens, xSize size) {
    pdre_calloc(xChar *str, size + 1, sizeof(xChar))
    for (xSize i = 0; i < size && tokens[i]; i++) {
        str[i] = tokens[i]->value[0];
    }
    str[size] = xChar('\0');
    return str;
}


#include "objects.h"
#include "xchar.h"
#include "xstring.h"

//typedef void * (*parse_rule)(token **, xSize *);
//parse_rule parse_rules[N_RULES];

static xSize assign_flag = 0;

#define PARSE_CASE(__item_name, __condition) \
    ReObj * __sub_obj = NULL; \
    while (tp[0] && (__condition)) { \
        xBool  only_parse = false; \
        xBool  is_inverse = false; \
        switch (tp[0]->catcode) {\
            case onlyParse: { \
                only_parse = true; \
                tp ++; \
            } \
            case inverse: { \
                is_inverse = true; \
                tp ++; \
            } \
            case beginG: { \
                __sub_obj = (ReObj *)parse_grp(tp, offs); \
                break; \
            } \
            case beginS: { \
                __sub_obj = (ReObj *)parse_set(tp, offs); \
                break; \
            } \
            case plain: { \
                __sub_obj = (ReObj *)parse_seq(tp, offs); \
                break; \
            } \
            case unionOR: { \
                if (n_subobjs != 1)  \
                    goto __failed_parse_##__item_name; \
                __sub_obj = (ReObj *)parse_uni_rhs(tp, offs); \
                if ( ! __sub_obj) \
                    goto __failed_parse_##__item_name; \
                if ( ((obj_type *) subobj_buffer[0])[0] == UNI \
                  && ((obj_type *) subobj_buffer[0])[0] == GRP) \
                    ((Union *) __sub_obj)->LHS = subobj_buffer[0];\
                else { \
                    pdre_malloc(Group * sub_grp, sizeof(Group)); \
                    sub_grp->id = GRP; \
                    sub_grp->n_sub = 1; \
                    sub_grp->rexp = show_processed_rexp(ttp, tp - ttp); \
                    pdre_calloc(sub_grp->subObjs, 2, sizeof(ReObj *))\
                    sub_grp->subObjs[0] = subobj_buffer[0];    \
                    addGrpToTable(sub_grp); \
                    ((Union *) __sub_obj)->LHS = (ReObj *) sub_grp;\
                } \
                n_subobjs = 0; \
                break; \
            } \
            case assign:{ \
                tp ++; \
                assign_flag = 1; \
                __sub_obj = (ReObj *)parse_var(tp, offs); \
                if ( ! __sub_obj) \
                    goto __failed_parse_##__item_name; \
                n_subobjs --; \
                ((Var *) __sub_obj)->obj = subobj_buffer[n_subobjs]; \
                break; \
            } \
            case call:{ \
                tp ++; \
                assign_flag = 0; \
                __sub_obj = (ReObj *)parse_var(tp, offs); \
                break; \
            } \
            case beginC:{           \
                __sub_obj = (ReObj *)parse_cnt(tp, offs); \
                if ( ! __sub_obj) \
                    goto __failed_parse_##__item_name; \
                n_subobjs --; \
                ((Count *)__sub_obj)->obj = subobj_buffer[n_subobjs]; \
                break; \
            } \
            default: \
                goto __failed_parse_##__item_name; \
        } \
        if (!__sub_obj) \
        goto __failed_parse_##__item_name; \
        __sub_obj->only_parse = only_parse; \
        __sub_obj->is_inverse = is_inverse; \
        while (n_subobjs >= subobj_buff_len) { \
        subobj_buff_len += ARRAY_ALLOC_SIZE; \
        pdre_realloc(subobj_buffer, subobj_buffer, (subobj_buff_len + 1) * sizeof(ReObj *)) \
        } \
        subobj_buffer[n_subobjs] = __sub_obj; \
        tp += *offs; \
        n_subobjs++; \
        only_parse = false; \
        is_inverse = false; \
    } \


Top *parse(token **tokens) {
    token **tp = tokens;
    xSize n_subobjs = 0;
    xSize subobj_buff_len = ARRAY_ALLOC_SIZE - 1;
    pdre_calloc(ReObj **subobj_buffer, subobj_buff_len + 1, sizeof(ReObj *))

    token ** ttp = tp;
    xSize offset = 0;
    xSize *offs = &offset;

    PARSE_CASE(top, true)

    pdre_realloc(subobj_buffer, subobj_buffer, (n_subobjs + 1) * sizeof(ReObj *))
    subobj_buffer[n_subobjs] = NULL;

    pdre_malloc(Top *top, sizeof(Top))
    top->id = TOP;
    top->rexp = show_processed_rexp(tokens, tp - tokens);
    top->n_sub = n_subobjs;
    top->subObjs = subobj_buffer;
    return top;

    __failed_parse_top:
    for (xSize i = 0; i < n_subobjs; i++)
        releaseObj(subobj_buffer[i]);
    pdre_free(subobj_buffer)
    return NULL;
}

Seq *parse_seq(token **tokens, xSize *offs) {
    token **tp = tokens;
    xSize seq_len = 0;
    xSize seq_buff_len = ARRAY_ALLOC_SIZE - 1;
    pdre_calloc(xChar *seq_buffer, seq_buff_len + 1, sizeof(xChar))
    for (; tp[seq_len] && tp[seq_len]->catcode == plain; seq_len++) {
        while (seq_len >= seq_buff_len) {
            seq_buff_len += ARRAY_ALLOC_SIZE;
            pdre_realloc(seq_buffer, seq_buffer, (seq_buff_len + 1) * sizeof(xChar))
        }
        seq_buffer[seq_len] = tp[seq_len]->value[0];
    }
    pdre_realloc(seq_buffer, seq_buffer, (seq_len + 1) * sizeof(xChar))
    seq_buffer[seq_len] = xChar('\0');
    *offs = seq_len;
    pdre_malloc(Seq *seq, sizeof(Seq))
    seq->id = SEQ;
    seq->len = seq_len;
    seq->value = seq_buffer;
    seq->value[seq_len] = xChar('\0');
    return seq;
}

Range *parse_rng(token **tokens, xSize *offs) {
    token **tp = tokens;
    if (!tp[0] || tp[0]->catcode != plain)
        goto __failed_parse_range;
    xChar start = tp[0]->value[0];
    tp++;

    if (!tp[0] || tp[0]->catcode != rangeTO)
        goto __failed_parse_range;
    tp++;

    if (!tp[0] || tp[0]->catcode != plain)
        goto __failed_parse_range;
    xChar end = tp[0]->value[0];
    tp++;

    if (ord(end) < ord(start))
        goto __failed_parse_range;

    pdre_calloc(Range *range, 1, sizeof(Range))
    range->id = RNG;
    range->start = start;
    range->end = end;
    *offs = tp - tokens;
    return range;

    __failed_parse_range:
    *offs = 0;
    return NULL;
}

Set *parse_set(token **tokens, xSize *offs) {
    token **tp = tokens;
    if (!tp[0] || tp[0]->catcode != beginS)
        goto __failed_parse_set;
    tp++;

    xSize plain_buff_len = ARRAY_ALLOC_SIZE - 1;
    xSize range_buff_len = ARRAY_ALLOC_SIZE - 1;
    pdre_malloc(xChar *plain_buffer, (plain_buff_len + 1) * sizeof(xChar))
    pdre_malloc(Range **range_buffer, (range_buff_len + 1) * sizeof(Range *))
    xSize n_plains = 0, n_ranges = 0;
    while (tp[0] && tp[0]->catcode != endS) {
        switch (tp[0]->catcode) {
            case plain: {
                while (n_plains >= plain_buff_len) {
                    plain_buff_len += ARRAY_ALLOC_SIZE;
                    pdre_realloc(plain_buffer, plain_buffer, (plain_buff_len + 1) * sizeof(xChar))
                }
                plain_buffer[n_plains] = tp[0]->value[0];
                n_plains++;
                *offs = 1;
                break;
            }
            case rangeTO: {
                tp--;
                n_plains--;
                while (n_ranges >= range_buff_len) {
                    range_buff_len += ARRAY_ALLOC_SIZE;
                    pdre_realloc(range_buffer, range_buffer, (range_buff_len + 1) * sizeof(Range *))
                }
                range_buffer[n_ranges] = parse_rng(tp, offs);
                if (!range_buffer[n_ranges])
                    goto __failed_parse_set;
                n_ranges++;
                break;
            }
            default:
                goto __failed_parse_set;
        }
        tp += *offs;
    }
    if (!tp[0] || tp[0]->catcode != endS)
        goto __failed_parse_set;
    tp++;

    pdre_realloc(plain_buffer, plain_buffer, (n_plains + 1) * sizeof(xChar))
    plain_buffer[n_plains] = xChar('\0');
    pdre_realloc(range_buffer, range_buffer, (n_ranges + 1) * sizeof(Range *))
    range_buffer[n_ranges] = NULL;
    pdre_malloc(Set *set, sizeof(Set))
    set->id = SET;
    set->n_plains = n_plains;
    set->plain = plain_buffer;
    set->n_ranges = n_ranges;
    set->ranges = range_buffer;
    *offs = tp - tokens;
    set->rexp = show_processed_rexp(tokens, *offs);
    return set;

    __failed_parse_set:
    *offs = 0;
    return NULL;
}

Group *parse_grp(token **tokens, xSize *offs) {
    token **tp = tokens;
    xSize n_subobjs = 0;
    xSize subobj_buff_len = ARRAY_ALLOC_SIZE - 1;
    pdre_calloc(ReObj **subobj_buffer, subobj_buff_len + 1, sizeof(ReObj *))

    if (!tp[0] || tp[0]->catcode != beginG)
        goto __failed_parse_group;
    tp++;
    token **ttp = tp;
    PARSE_CASE(group, tp[0]->catcode != endG)

    if (!tp[0] || tp[0]->catcode != endG)
        goto __failed_parse_group;
    tp++;

    pdre_malloc(Group *grp, sizeof(Group))
    addGrpToTable(grp);

    grp->id = GRP;
    grp->n_sub = n_subobjs;
    grp->subObjs = subobj_buffer;
    *offs = tp - tokens;
    grp->rexp = show_processed_rexp(tokens, *offs);
    return grp;

    __failed_parse_group:
    *offs = 0;
    for (xSize i = 0; i < n_subobjs; i++)
        releaseObj(subobj_buffer[i]);
    pdre_free(subobj_buffer)
    return NULL;
}


Union *parse_uni_rhs(token **tokens, xSize *offs) {
    token **tp = tokens;
    if (!tp[0] || tp[0]->catcode != unionOR)
        goto __failed_parse_uni_rhs;
    tp++;
    Group *rhs = parse_grp(tp, offs);
    if (!rhs)
        goto __failed_parse_uni_rhs;
    tp += *offs;
    pdre_calloc(Union *uni, 1, sizeof(Union))
    uni->id = UNI;
    uni->RHS = rhs;
    *offs = tp - tokens;
    return uni;

    __failed_parse_uni_rhs:
    *offs = 0;
    return NULL;
}

Count *parse_cnt(token **tokens, xSize *offs) {
    token **tp = tokens;
    xSize min = 0;
    xSize max = 0;
    xSize len = 0;
    xSize num_buff_len = ARRAY_ALLOC_SIZE - 1;
    pdre_malloc(xChar *num_buffer, (num_buff_len + 1) * sizeof(xChar))

    if (!tp[0] || tp[0]->catcode != beginC)
        goto __failed_parse_count;
    tp++;

    while (tp[0] && tp[0]->catcode == plain) {
        while (len >= num_buff_len) {
            num_buff_len += ARRAY_ALLOC_SIZE;
            pdre_realloc(num_buffer, num_buffer, (num_buff_len + 1) * sizeof(xChar))
        }
        num_buffer[len] = tp[0]->value[0];
        tp++;
        len++;
    }
    if (len != 0) {
        num_buffer[len] = xChar('\0');
        if (!str2ul(num_buffer, &min, xChar('\0'), 10))
            goto __failed_parse_count;
    }

    if (tp[0]->catcode != comma)
        goto __failed_parse_count;
    tp++;

    len = 0;
    while (tp[0] && tp[0]->catcode == plain) {
        while (len >= num_buff_len) {
            num_buff_len += ARRAY_ALLOC_SIZE;
            pdre_realloc(num_buffer, num_buffer, (num_buff_len + 1) * sizeof(xChar))
        }
        num_buffer[len] = tp[0]->value[0];
        tp++;
        len++;
    }
    if (len != 0) {
        num_buffer[len] = xChar('\0');
        if (!str2ul(num_buffer, &max, xChar('\0'), 10))
            goto __failed_parse_count;
    }
    if (!tp[0] || tp[0]->catcode != endC)
        goto __failed_parse_count;
    tp++;

    pdre_calloc(Count *cnt, 1, sizeof(Count))
    cnt->id = CNT;
    cnt->min = min;
    cnt->max = max;
    pdre_free(num_buffer)
    *offs = tp - tokens;
    return cnt;

    __failed_parse_count:
    pdre_free(num_buffer)
    *offs = 0;
    return NULL;
}

Var *parse_var(token **tokens, xSize *offs) {
    token **tp = tokens;

    xSize vname_len = 0;
    xSize vname_buff_len = ARRAY_ALLOC_SIZE - 1;
    pdre_malloc(xChar *var_name, (vname_buff_len + 1) * sizeof(xChar))

    if (!tp[0] || tp[0]->catcode != beginV)
        goto __failed_parse_var;
    tp++;

    while (tp[0]->catcode != endV) {
        if (tp[0]->catcode != plain)
            goto __failed_parse_var;
        while (vname_len >= vname_buff_len) {
            vname_buff_len += ARRAY_ALLOC_SIZE;
            pdre_realloc(var_name, var_name, (vname_buff_len + 1) * sizeof(xChar))
        }
        var_name[vname_len] = tp[0]->value[0];
        vname_len++;
        tp++;
    }
    if (!tp[0] || tp[0]->catcode != endV)
        goto __failed_parse_var;
    tp++;

    pdre_realloc(var_name, var_name, (vname_len + 1) * sizeof(xChar))
    var_name[vname_len] = xChar('\0');

    xSize var_count = getVarCount();
    for (xSize i = 0; i < var_count; i++) {
        Var *p = getVar(i);
        if (strcmp(p->value, var_name, 0) == 0) {
            *offs = tp - tokens;
            pdre_free(var_name)
            return p;
        }
    }

    if (assign_flag) {
        pdre_calloc(Var *var, 1, sizeof(Var))
        var->id = VAR;
        var->value = var_name;
        addVarToTable(var);
        *offs = tp - tokens;
        return var;
    }
    __failed_parse_var:
    pdre_free(var_name)
    *offs = 0;
    return NULL;
}

//parse_rule parse_rules[N_PREPROCESS] = {
//        (parse_rule) parse_seq,
//        (parse_rule) parse_rng,
//        (parse_rule) parse_grp,
//        (parse_rule) parse_set,
//        (parse_rule) parse_uni_rhs,
//};
