//
// Created by "Yaokai Liu" on 12/10/22.
//

#include "catcode.h"
#include "xdef.h"

// This part of 'CAT_OF_CHARS' is terrible.
// Maybe there is some better ways to make this job.
static const catcode CAT_OF_CHARS[128] = {
        [escape]    = escape,
        [beginG]    = beginG,
        [endG]      = endG,
        [beginS]    = beginS,
        [endS]      = endS,
        [beginC]    = beginC,
        [endC]      = endC,
        [beginV]    = beginV,
        [endV]      = endV,
        [rangeTO]   = rangeTO,
        [comma]     = comma,
        [unionOR]   = unionOR,
        [inverse]   = inverse,
        [assign]    = assign,
        [call]      = call,
        [numOf]     = numOf,
        [condWith]  = condWith,
        [onlyParse] = onlyParse,
};

catcode get_catcode(xChar _dest) {
    xSize e = ord(_dest) - ord(xChar('\0'));
    if (e > alias - 1)
        return plain;
    if (CAT_OF_CHARS[e])
        return CAT_OF_CHARS[e];
    switch (_dest) {
        case xChar('*'):
        case xChar('+'):
        case xChar('?'):
            return alias;
        default:
            return plain;
    }
}


// This part of 'ALIAS_OF_CHARS' is terrible.
// Maybe there is some better ways to make this job.
xChar *get_alias(const xChar *_dest, xSize *offs) {
    switch (*_dest) {
        case xChar('*'): {
            *offs = 1;
            return xChar("{,}");
        }
        case xChar('+'): {
            *offs = 1;
            return xChar("{1,}");
        }
        case xChar('?'): {
            *offs = 1;
            return xChar("{,1}");
        }
    }
    *offs = 0;
    return NULL;
}
