/*
MIT License

Copyright (c) 2022 Yaokai Liu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
//
// Created by Yaokai Liu on 22-10-10.
//

#include "alloc.h"
#include <stdio.h>

#ifdef DEBUG

MemEntity header = (MemEntity){};

const MemEntity * const MemEntityHeader = &header;
MemEntity * CurMemEntity = &header;
xSize  total_mem_entity = 0;


void * __pdre_malloc(char * _var, xSize _size, char * file, xSize line) {
    CurMemEntity->next = calloc(1, sizeof(MemEntity));
    CurMemEntity->next->file = file;
    CurMemEntity->next->line = line;
    CurMemEntity->next->name = _var;
    CurMemEntity->next->addr = malloc(_size);
    CurMemEntity->next->size = _size;
    CurMemEntity->next->is_free = 0;
    CurMemEntity = CurMemEntity->next;
    total_mem_entity ++;
    return CurMemEntity->addr;
}

void * __pdre_calloc(char * _var, xSize _nmemb,xSize _size, char * file, xSize line) {
    CurMemEntity->next = calloc(1, sizeof(MemEntity));
    CurMemEntity->next->file = file;
    CurMemEntity->next->line = line;
    CurMemEntity->next->name = _var;
    CurMemEntity->next->addr = calloc(_nmemb, _size);
    CurMemEntity->next->size = _nmemb * _size;
    CurMemEntity->next->is_free = 0;
    CurMemEntity = CurMemEntity->next;
    total_mem_entity++;
    return CurMemEntity->addr;
}

void *__pdre_realloc(char * _var1, void * src, xSize _size, char * file, xSize line) {
    MemEntity *__pp = NULL;
    for (__pp = (MemEntity *) MemEntityHeader;
         __pp != NULL; __pp = __pp->next) {
        if (__pp->addr == src) {
            break;
        }
    }
    if (!__pp)
        return NULL;
    __pp->file = file;
    __pp->line = line;
    __pp->name = _var1;
    __pp->addr = realloc(__pp->addr, _size);
    __pp->size = _size;
    __pp->is_free = 0;
    return __pp->addr;
}


void __pdre_free(void * _var) {
    for (MemEntity * __pp = (MemEntity *) MemEntityHeader;
                __pp->next != NULL; __pp = __pp->next) {
        if (__pp->next->addr == _var) {
            MemEntity *q = __pp->next;
            __pp->next = q->next;
            if (q == CurMemEntity)
                CurMemEntity = __pp;
            free(q->addr);
            free(q);
            total_mem_entity -= 1;
            break;
        }
    }
}


void print_all_entity() {
    if (total_mem_entity > 0)
        fprintf(stdout, "Count of unfree entity: %zu\n", total_mem_entity);
    xSize i = 1;
    for(MemEntity * _pp = MemEntityHeader -> next; _pp != NULL; _pp = _pp->next, i ++){
        fprintf(stdout, "%zu: File '%s', line '%zu', Variable '%s', address @<%llx>, size=%zu\n",
                i, _pp->file, _pp->line, _pp->name, (long long) _pp->addr, _pp->size);
    }
}

void print_unfree_entity() {
    if (total_mem_entity > 0)
        fprintf(stderr, "Count of unfree entity: %zu\n", total_mem_entity);
    xSize i = 1;
    for(MemEntity * _pp = MemEntityHeader -> next; _pp != NULL; _pp = _pp->next, i ++){
        fprintf(stderr, "%zu: File '%s', line '%zu', Variable '%s', address @<%llx>, size=%zu\n",
                i, _pp->file, _pp->line, _pp->name, (long long) _pp->addr, _pp->size);
    }
}

#endif