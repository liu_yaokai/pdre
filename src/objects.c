/*
MIT License

Copyright (c) 2022 Yaokai Liu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Yaokai Liu on 22-10-1.
//

#include "objects.h"
#include "alloc.h"

static Var ** var_table    = NULL;
static Group ** grp_table  = NULL;

static xSize var_count = 0;
static xSize grp_count = 0;
static xSize var_table_len = 0;
static xSize grp_table_len = 0;

inline xSize getVarCount() {
    return var_count;
}

inline xSize getGrpCount() {
    return grp_count;
}

Var * getVar(xSize _index) {
    return var_table[_index];
}
Group * getGrp(xSize _index) {
    return grp_table[_index];
}

xSize addVarToTable(Var * var) {
    while (var_count >= var_table_len) {
        var_table_len += ARRAY_ALLOC_SIZE;
        if (var_table)
            pdre_realloc(var_table, var_table, var_table_len * sizeof(Var *))
        else
            pdre_malloc(var_table, var_table_len * sizeof(Var *))
    }
    var_table[var_count] = var;
    var_count ++;
    return 0;
}
xSize addGrpToTable(Group * grp) {
    while (grp_count >= grp_table_len) {
        grp_table_len += ARRAY_ALLOC_SIZE;
        if (grp_table)
            pdre_realloc(grp_table, grp_table, grp_table_len * sizeof(Group *))
        else
            pdre_malloc(grp_table, grp_table_len * sizeof(Group *))
    }
    grp_table[grp_count] = grp;
    grp_count ++;
    return 0;
}

void releasepTokenArray(token * tokens[]) {
    for (xSize i = 0; tokens[i]; i ++) {
        pdre_free(tokens[i])
    }
    pdre_free(tokens)
}

void releaseSeq(Seq * seq);

void releaseGrp(Group *grp);

void releaseSet(Set *set);

void releaseUni(Union *uni);

void releaseTop(Top *top);

void releaseCnt(Count *cnt);

void releaseVar(Var *var);

void releaseObj(void *obj) {
    obj_type id = ((obj_type *) obj)[0];
    switch (id) {
        case SEQ: {
            releaseSeq(obj);
            break;
        }
        case GRP: {
            releaseGrp(obj);
            break;
        }
        case SET: {
            releaseSet(obj);
            break;
        }
        case UNI: {
            releaseUni(obj);
            break;
        }
        case TOP: {
            releaseTop(obj);
            break;
        }
        case CNT: {
            releaseCnt(obj);
            break;
        }
        case VAR: {
            releaseVar(obj);
            break;
        }
        default: {
            pdre_free(obj)
        }
    }
}

void releaseSeq(Seq * seq) {
    pdre_free(seq->value)
    pdre_free(seq)
}

void releaseGrp(Group *grp) {
    for (xSize i = 0; i < grp->n_sub; i++)
        releaseObj(grp->subObjs[i]);
    pdre_free(grp->subObjs)
    pdre_free(grp->rexp)
    pdre_free(grp)
}

void releaseSet(Set *set) {
    for (xSize i = 0; i < set->n_ranges; i++) {
        releaseObj(set->ranges[i]);
    }
    if (set->ranges){
        pdre_free(set->ranges)
    }
    if (set->plain) {
        pdre_free(set->plain)
    }
    pdre_free(set->rexp)
    pdre_free(set)
}

void releaseUni(Union *uni) {
    releaseObj(uni->LHS);
    releaseGrp(uni->RHS);
    pdre_free(uni)
}

void releaseTop(Top *top) {
    for (xSize i = 0; i < top->n_sub; i++)
        releaseObj(top->subObjs[i]);
    pdre_free(top->subObjs)
    pdre_free(top->rexp)
    pdre_free(top)
}

void releaseCnt(Count *cnt) {
    releaseObj(cnt->obj);
    pdre_free(cnt)
}

void releaseVar(Var *var) {
    pdre_free(var->value)
    releaseObj(var->obj);
    pdre_free(var)
}

void releaseVarTable() {
    var_table_len = 0;
    pdre_free(var_table)
    var_count = 0;
}

void releaseGrpTable() {
    grp_table_len = 0;
    pdre_free(grp_table)
    grp_count = 0;
}