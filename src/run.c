/*
MIT License

Copyright (c) 2022 Yaokai Liu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
//
// Created by Yaokai Liu on 22-10-12.
//

#include "run.h"
#include "xdef.h"

xBool  match_seq(const Seq * seq, const xChar * src, xSize * __n_matched) {
    for(xSize i = 0; seq->value[i] && src[i] != xChar('\0'); i ++, (*__n_matched) ++) {
        if ((seq->value[i] == src[i]) == seq->is_inverse)
            goto __failed_match;
    }
    return true;

    __failed_match:
    return false;
}

xBool  match_set(const Set * set, const xChar * src, xSize * __n_matched) {
    for(xSize i = 0; set->plain[i]; i ++) {
        if (set->plain[i] == src[0]) {
            *__n_matched = !set->is_inverse;
            return *__n_matched;
        }
    }
    for (xSize j = 0; j < set->n_ranges; j ++){
        if (ord(set->ranges[j]->start) <= ord(src[0])
          && ord(set->ranges[j]->end) >= ord(src[0])) {
            *__n_matched = !set->is_inverse;
            return *__n_matched;
        }
    }
    return set->is_inverse;
}

xBool  match_grp(const Group * grp, const xChar * src, xSize * __n_matched) {
    xSize matched = 0;
    for(xSize i = 0; i < grp->n_sub; i ++) {
        if (grp->subObjs[i]->only_parse)
            continue;
        xBool  b = match_obj(grp->subObjs[i], src + matched, __n_matched);
        if (b == grp->is_inverse)
            goto __failed_match;
        matched += *__n_matched;
    }
    *__n_matched = matched;
    return true;

    __failed_match:
    *__n_matched = matched;
    return false;
}

xBool  match_uni(const Union * uni, const xChar * src, xSize * __n_matched) {
    xSize result = match_obj(uni->RHS, src, __n_matched);
    if (result == false)
        result = match_obj(uni->LHS, src, __n_matched);
    return (result != uni->is_inverse);
}

xBool  match_cnt(const Count * cnt, const xChar * src, xSize * __n_matched) {
    xSize result = 0;
    for (xSize i = 0; i < cnt->min; ++i) {
        if (!match_obj(cnt->obj, src + result, __n_matched))
            goto __failed_match;
        result += *__n_matched;
    }
    for (xSize i = cnt->min; (i < cnt->max || cnt->max == 0); ++i) {
        if (!match_obj(cnt->obj, src + result, __n_matched))
            break;
        result += *__n_matched;
    }
    *__n_matched = result;
    return !cnt->is_inverse;

    __failed_match:
    *__n_matched = result;
    return cnt->is_inverse;
}

xBool  match_var(const Var * var, const xChar * src, xSize * __n_matched) {
    return var->is_inverse != match_obj(var->obj, src, __n_matched);
}

xBool  match_top(const Top * top, const xChar * src, xSize * __n_matched) {
    xSize result = 0;
    for(xSize i = 0; i < top->n_sub; i ++) {
        if (top->subObjs[i]->only_parse)
            continue;
        xBool  b = match_obj(top->subObjs[i], src + result, __n_matched);
        if (!b)
            goto __failed_match;
        result += *__n_matched;
    }
    *__n_matched = result;
    return true;

    __failed_match:
    *__n_matched = result;
    return false;
}


xBool  match_obj(const void * obj, const xChar * src, xSize * __n_matched) {
    switch (((obj_type *)obj)[0]) {
        case SEQ:
            return match_seq(obj, src, __n_matched);
        case SET:
            return match_set(obj, src, __n_matched);
        case GRP:
            return match_grp(obj, src, __n_matched);
        case UNI:
            return match_uni(obj, src, __n_matched);
        case CNT:
            return match_cnt(obj, src, __n_matched);
        case VAR:
            return match_var(obj, src, __n_matched);
        case TOP:
            return match_top(obj, src, __n_matched);
        default:
            return false;
    }
}
