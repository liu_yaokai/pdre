/*
MIT License

Copyright (c) 2022 Yaokai Liu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
//
// Created by Yaokai Liu on 22-10-10.
//

#include "parse.h"
#include "pdre.h"
#include "xdef.h"
#ifdef DEBUG
#include "alloc.h"
#include "xstring.h"

#endif

/*
 *  pdre -c [<rexp1>, ...] [options]
 *      compile [<rexp1>, ...] to library.
 *      options:
 *          [-o, --output]      specify path and filename of the library.
 *          [-d, --dynamic]     dynamic library of using this option.
 *
 *  pdre -f <rexp> <string> [options]
 *      find positions in <string> where matchable with <rexp>.
 *      default return only the first one.
 *      options:
 *          [-n]                max times of match, return in a list.
 *          [-a, --all]         find all, return in a list.
 *
 *  pdre -e <rexp> <string>
 *      check if rexp is matchable for string.
 *
 *  pdre -r <rexp> <src_string> <str2> [options]
 *      replace substrings where matchable with <rexp> in <src_string> with <str2>.
 *      default replace only the first one.
 *      options:
 *          [-n]                max times of match, return in a list.
 *          [-a, --all]         find all, return in a list.
 */
int main(int argc, char * argv[]) {

}

xBool  pdre_match(const xChar * rexp, const xChar * src, xSize * __n_matched){
    token ** tokens = tokenize(rexp, NULL);
    Top * top = parse(tokens);
    xBool  result = match_top(top, src, __n_matched);
    pdre_free(tokens)
    pdre_free(top)
    return result;
}

xSize pdre_find(const xChar * rexp, const xChar * src, xBool  * __found, xSize * __n_matched) {
    * __found = false;
    xSize index = 0;
    while (src[index] != xChar('\0')){
        if(!(pdre_match(rexp, src + index, __n_matched)))
            index += * __n_matched;
    }
    return src[index] == xChar('\0');
}

xChar * pdre_replace(const xChar * rexp, xChar * src, const xChar * __b, xBool  * __found) {
    xSize start = 0;
    xSize len_a = 0;
    xBool  found = false;
    start = pdre_find(rexp, src, &found, &len_a);
    if (! found) {
        * __found = false;
        return src;
    }
    xSize len_b = strlen(__b, 0);
    xSize len_s = strlen(src, 0);
    xSize len = ((len_b > len_a) ? len_b : len_a);
    xSize final_len = len_s - len_a + len + 1;

    pdre_realloc(src, src, (final_len + 1) * sizeof(xChar))
    src[final_len - 1] = xChar('\0');
    xChar *sp = src + start + len_a;
    for (xSize i = 0; i < (final_len - start - len_a) / 2; i++) {
        xChar temp = sp[i];
        sp[i] = src[final_len - 1 - i];
        src[final_len - 1 - i] = temp;
    }
    sp = src + start + len_b;
    for (xSize i = 0; i < (final_len - start - len_b) / 2; i++) {
        xChar temp = sp[i];
        sp[i] = src[final_len - 1 - i];
        src[final_len - 1 - i] = temp;
    }
    sp = src + start;
    for (xSize i = 0; i < len_b; i++) {
        sp[i] = __b[i];
    }
    final_len = strlen(src, 0);
    pdre_realloc(src, src, (final_len + 1) * sizeof(xChar))
    src[final_len] = xChar('\0');
    * __found = true;
    return src;
}